# Generate a Usage Report for Accounting

A collection of scripts to determine license fees for a [Tocco](https://www.tocco.ch/software/branchenlosungen/ubersicht)
installations.

## Structure

* <stats>.sql: SQL generating the stats

  {{{installation}}} placeholder is replaced with the installations name
* <stats>.epilogue: Text printed before the stats

Available <stats> are listed below.

## Usage

./run [-h|--help] [-q|--quiet] <stats> [installation]...

* stats: The SQL query to execute without the .sql extension.
* installation: Installation against which to run. All prod systems if omitted.

Use csvtools to get human-readable output:

```
./run --quiet <stats> [installation]... | csvtool readable -
```

or

```
csvtool readable <csv_file>
```

## Available Stats

### admin_users

List of all users / people that …
* … have an *manager* role
* … have an active login
* … are not @tocco.ch logins

Every row corresponds to a login rather than a person. A person with multiple
logins will be listed multiple times with the same *user no* but incrementing
*login no*.

### admin_users_simple

Like admin_users but only prints a simple count indicating the total number
admin users.

### emails

Outgoing mails per month averaged over the last 12 months (if possible).

* For simplicity a year is assumed to be 360 days and a month 30 days.
* If the first mail ever sent has been sent less than 12 months ago, the time span
  since the first mail is used to calculate a monthly average. The result is
  extrapolated if necessary.

### memory

Use this to get per-installaiton memory:

```
tco memory --csv
```

### storage

Use this to get used DB, Elasticsearch and Solr storage:

```
cd ${ANSIBLE_REPOSITORY}/tocco
ansible-playbook playbooks/storage_usage.yml -o output=storage.csv
```

Then grab file at *playbooks/storage.csv*.

Elasticsearch stats are also available via:

```
tco elasticsearch index-stats --csv
```
