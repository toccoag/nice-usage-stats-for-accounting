SELECT
     '{{{installation}}}' AS "installation",
     COUNT(DISTINCT u.pk) AS "count"
   FROM
     nice_user AS u
     LEFT OUTER JOIN
     nice_principal AS p ON u.pk = p.fk_user
     LEFT OUTER JOIN
     nice_login_role AS l ON p.pk = l.fk_principal
     LEFT OUTER JOIN
     nice_role AS r ON l.fk_role = r.pk
     LEFT OUTER JOIN
     nice_role_type AS t ON r.fk_role_type = t.pk
     LEFT OUTER JOIN
     nice_principal_status AS s ON p.fk_principal_status = s.pk
   WHERE
     t.unique_id = 'manager' AND s.unique_id = 'active'
       AND u.email NOT LIKE '%@tocco.ch';
