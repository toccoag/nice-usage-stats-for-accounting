SELECT
     '{{{installation}}}' AS installation,
     dense_rank() OVER (ORDER BY u.firstname, u.lastname, u.pk) AS "person no.",
     row_number() OVER (PARTITION BY u.pk ORDER BY p.username) AS "login no.",
     u.firstname AS "first name",
     u.lastname AS "last name",
     u.email AS email,
     p.username AS "user name",
     (SELECT count(*) FROM nice_api_key AS key WHERE p.pk = key.fk_principal) AS "API key count",
     p.last_login::date AS "last login",
     u.pk AS "person PK",
     p.pk AS "login PK",
     'https://{{{installation}}}.tocco.ch/tocco/e/User/' || u.pk::text AS "person",
     'https://{{{installation}}}.tocco.ch/tocco/e/Principal/' || p.pk::text AS "login"
   FROM
     nice_user AS u
     LEFT OUTER JOIN
     nice_principal AS p ON u.pk = p.fk_user
     LEFT OUTER JOIN
     nice_login_role AS l ON p.pk = l.fk_principal
     LEFT OUTER JOIN
     nice_role AS r ON l.fk_role = r.pk
     LEFT OUTER JOIN
     nice_role_type AS t ON r.fk_role_type = t.pk
     LEFT OUTER JOIN
     nice_principal_status AS s ON p.fk_principal_status = s.pk
   WHERE
     t.unique_id = 'manager'
       AND s.unique_id = 'active'
       AND u.email NOT LIKE '%@tocco.ch'
   GROUP BY
     p.pk, p.username, u.pk, u.firstname, u.lastname, u.email
   ORDER BY
     u.firstname, u.lastname, u.pk, p.username;
