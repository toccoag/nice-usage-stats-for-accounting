SELECT
     '{{{installation}}}' AS "installation",
     (COUNT(*)
      -- time since first mail has been sent, clamped at 360 days.
      / extract(EPOCH FROM (SELECT least(interval '360d', max(now() - timestamp)) FROM nice_email_archive))
      -- 30 days in seconds
      * 2592000)::int AS "count"
   FROM
     nice_email_archive AS ea
   WHERE
     ea.timestamp >= now() - interval '360d'
     AND original_mail = ''; -- left blank on outgoing mail
